package manager;

import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import ant.flow.SocialApplication;


public class ParseManager {

	static ParseManager parseManager = null;

	public static ParseManager getParseManager() {
		if (parseManager == null)
			parseManager = new ParseManager();
		return parseManager;
	}

	public void signupUser(String firstname, String password, String email,
			String lastname) {
		ParseUser user = new ParseUser();
		user.setUsername(email);
		user.setPassword(password);
		user.setEmail(email);
		user.put("name", firstname + " " + lastname);

		user.signUpInBackground(new SignUpCallback() {

			@Override
			public void done(ParseException exception) {

				if (exception != null) {
					exception.printStackTrace();
				} else {
					Log.v(SocialApplication.TAG, "Sign up done");
				}
			}
		});
	}
}
