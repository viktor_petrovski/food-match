package manager;

import android.os.Bundle;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

public class FacebookManager {

	static String displayNameTAG = "name";
	static String userEmailTAG = "email";
	static String userIDTAG = "id";
	static String userProfilePictureurl = "url";

	// Singletone istance
	static FacebookManager facebookManager = null;

	public static FacebookManager getFBManager() {
		if (facebookManager == null)
			facebookManager = new FacebookManager();
		return facebookManager;
	}

	public void getLoggedinUserInfo() {
		new Request(Session.getActiveSession(), "/me", null, HttpMethod.GET,
				new Request.Callback() {
					public void onCompleted(Response response) {
						JSONObject json;
						try {
							json = new JSONObject(response.getRawResponse());
							final String displayName = json
									.getString(displayNameTAG);
							final String userID = json.getString(userIDTAG);
							getLoggedinUserProfilePicture(new FBUserProfilePicutreCallback() {

								@Override
								public void getProfilePicutreURL(String url) {
									ParseUser loggedinUser = ParseUser
											.getCurrentUser();
									loggedinUser
											.put("displayName", displayName);
									loggedinUser.put("profilePic", url);
									loggedinUser.put("facebookID", userID);
									loggedinUser
											.saveInBackground(new SaveCallback() {

												@Override
												public void done(
														ParseException exception) {
													if (exception != null)
														exception
																.printStackTrace();
												}
											});
								}
							});
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}).executeAsync();

	}

	public void getLoggedinUserProfilePicture(
			final FBUserProfilePicutreCallback callback) {
		Bundle params = new Bundle();
		params.putBoolean("redirect", false);
		params.putString("height", "250");
		params.putString("type", "normal");
		params.putString("width", "250");
		/* make the API call */
		new Request(Session.getActiveSession(), "/me/picture", params,
				HttpMethod.GET, new Request.Callback() {
					public void onCompleted(Response response) {
						JSONObject jsonObject;
						try {
							jsonObject = new JSONObject(response
									.getRawResponse());
							JSONObject dataJson = jsonObject
									.getJSONObject("data");
							String userProfilePicUrl = dataJson
									.getString(userProfilePictureurl);
							callback.getProfilePicutreURL(userProfilePicUrl);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}).executeAsync();
	}

}

interface FBUserProfilePicutreCallback {
	void getProfilePicutreURL(String url);
}
