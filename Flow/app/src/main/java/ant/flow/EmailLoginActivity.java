package ant.flow;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import manager.ParseManager;


public class EmailLoginActivity extends BaseActivity {

	EditText firstName;
	EditText lastName;
	EditText email;
	EditText password;
	Button signUp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		setContentView(R.layout.login_email);
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void initComponenets() {
		firstName = (EditText) findViewById(R.id.et_FirstName);
		lastName = (EditText) findViewById(R.id.et_LastName);
		email = (EditText) findViewById(R.id.et_Email);
		password = (EditText) findViewById(R.id.et_Password);
		signUp = (Button) findViewById(R.id.btn_signUp);
		signUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				signUp();
			}
		});
	}

	private void signUp() {
		try {
			validateFields();
		} catch (InvalidInputFieldsException e) {
			e.printStackTrace();
		}

	}

	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	private void validateFields() throws InvalidInputFieldsException {
		// TODO:Implement validation for all componenets

		// Temporary validation only check if they are not nulll
		String emailString = this.email.getText().toString();
		String firstnameString = this.firstName.getText().toString();
		String lastnameString = this.lastName.getText().toString();
		String passwordString = this.password.getText().toString();

		if (isEmailValid(emailString) == false)
			throw new InvalidInputFieldsException("Email");
		if (firstnameString == null || lastnameString == null
				|| passwordString == null)
			throw new InvalidInputFieldsException();

		ParseManager.getParseManager().signupUser(firstnameString,
				passwordString, emailString, lastnameString);

	}

}

@SuppressWarnings("serial")
class InvalidInputFieldsException extends Exception {

	public InvalidInputFieldsException() {
		super("Some of the required fields are not insert properly ");
	}

	public InvalidInputFieldsException(String msg) {
		super("Field " + msg
				+ " is not valid, either is empty or failed to validate");
	}
}
