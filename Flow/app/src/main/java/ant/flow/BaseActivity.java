package ant.flow;

import android.app.Activity;
import android.os.Bundle;

public abstract class BaseActivity extends Activity {

	public static final String TAG = "MyApp";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initComponenets();

	}

	protected abstract void initComponenets();

}
