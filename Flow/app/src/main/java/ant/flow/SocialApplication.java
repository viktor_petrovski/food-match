package ant.flow;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;

public class SocialApplication extends Application {

	public static final String TAG = "MyApp";

	@Override
	public void onCreate() {
		super.onCreate();

		Parse.initialize(this, "l3RC05qYICZ8ijCArAA1eiWzNLJJA2RNFZrCuZ93",
				"38Ofh3T8LpB8XXa46xh1GiniLpDVu46pdSPITLU1");

		// Set your Facebook App Id in strings.xml
		ParseFacebookUtils.initialize(getString(R.string.app_id));
	}
}
