package com.nasaspaceapps.foodmatch;

/**
 * Created by sofijahristova on 10.4.15.
 */
public class Country {

   private String countryName;
   private String countryFlagUrl;

   private long distributionCorn = 0;
   private long distributionWheat= 0;
   private long distributionPalmOil= 0;
   private long distributionRice= 0;

   private long potentionalCorn= 0;
   private long potentionalWheat= 0;
   private long potentionalPalmOil= 0;
   private long potentionalRice= 0;

   private long currentProdCorn= 0;
   private long currentProdWheat= 0;
   private long currentProdPalmOil= 0;
   private long currentProdRice= 0;


  public static final String TYPE_RICE = "Rice";
  public static final String TYPE_CORN = "Corn";
  public static final String TYPE_WHEAT = "Wheat";
  public static final String TYPE_PALM_OIL = "Palm_oil";


    public Country(String countryName,String countryFlagUrl,long distributionCorn,long distributionPalmOil,long distributionRice,long distributionWheat){
        this.countryName = countryName;
        this.countryFlagUrl = countryFlagUrl;
        this.distributionCorn = distributionCorn;
        this.distributionPalmOil = distributionPalmOil;
        this.distributionWheat = distributionWheat;
        this.distributionRice = distributionRice;
    }

    //Constructor for countries that need help
    public Country(String countryName,String countryFlagUrl,long currentProdCorn,long currentProdPalmOil,
                   long currentProdRice,long currentProdWheat,
                   long potentionalCorn,long potentionalPalmOil,long potentionalRice,long potentionalWheat){

       this.countryName = countryName;
       this.countryFlagUrl = countryFlagUrl;

       this.currentProdCorn = currentProdCorn;
       this.currentProdPalmOil = currentProdPalmOil;
       this.currentProdRice = currentProdRice;
       this.currentProdWheat     = currentProdWheat;

       this.potentionalCorn      = potentionalCorn;
       this.potentionalPalmOil   = potentionalPalmOil;
       this.potentionalRice      = potentionalRice;
       this.potentionalWheat     = potentionalWheat;
    }

    public String getCountryFlagUrl() {
        return countryFlagUrl;
    }

    public String getCountryName() {
        return countryName;
    }

    @Override
    public String toString() {
        return getCountryName();
    }


    public long getCurrentProdRice() {
        return currentProdRice;
    }

    public void setCurrentProdRice(long currentProdRice) {
        this.currentProdRice = currentProdRice;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setCountryFlagUrl(String countryFlagUrl) {
        this.countryFlagUrl = countryFlagUrl;
    }

    public long getDistributionCorn() {
        return distributionCorn;
    }

    public void setDistributionCorn(long distributionCorn) {
        this.distributionCorn = distributionCorn;
    }

    public long getDistributionWheat() {
        return distributionWheat;
    }

    public void setDistributionWheat(long distributionWheat) {
        this.distributionWheat = distributionWheat;
    }

    public long getDistributionPalmOil() {
        return distributionPalmOil;
    }

    public void setDistributionPalmOil(long distributionPalmOil) {
        this.distributionPalmOil = distributionPalmOil;
    }

    public long getDistributionRice() {
        return distributionRice;
    }

    public void setDistributionRice(long distributionRice) {
        this.distributionRice = distributionRice;
    }

    public long getPotentionalCorn() {
        return potentionalCorn;
    }

    public void setPotentionalCorn(long potentionalCorn) {
        this.potentionalCorn = potentionalCorn;
    }

    public long getPotentionalWheat() {
        return potentionalWheat;
    }

    public void setPotentionalWheat(long potentionalWheat) {
        this.potentionalWheat = potentionalWheat;
    }

    public long getPotentionalPalmOil() {
        return potentionalPalmOil;
    }

    public void setPotentionalPalmOil(long potentionalPalmOil) {
        this.potentionalPalmOil = potentionalPalmOil;
    }

    public long getPotentionalRice() {
        return potentionalRice;
    }

    public void setPotentionalRice(long potentionalRice) {
        this.potentionalRice = potentionalRice;
    }

    public long getCurrentProdCorn() {
        return currentProdCorn;
    }

    public void setCurrentProdCorn(long currentProdCorn) {
        this.currentProdCorn = currentProdCorn;
    }

    public long getCurrentProdWheat() {
        return currentProdWheat;
    }

    public void setCurrentProdWheat(long currentProdWheat) {
        this.currentProdWheat = currentProdWheat;
    }

    public long getCurrentProdPalmOil() {
        return currentProdPalmOil;
    }

    public void setCurrentProdPalmOil(long currentProdPalmOil) {
        this.currentProdPalmOil = currentProdPalmOil;
    }

    public long avg(String type){
        switch (type){
            case TYPE_CORN:
              return getPotentionalCorn() - getCurrentProdCorn();
            case TYPE_WHEAT:
                return getPotentionalWheat() - getCurrentProdWheat();
            case TYPE_PALM_OIL:
                return getPotentionalPalmOil() - getCurrentProdPalmOil();
            case TYPE_RICE:
                return getPotentionalRice() - getCurrentProdRice();
        }
        return 0;
    }

    public long getDistribution(String type){
        switch (type){
            case TYPE_CORN:
                return getDistributionCorn();
            case TYPE_WHEAT:
                return getDistributionWheat();
            case TYPE_PALM_OIL:
                return getDistributionPalmOil();
            case TYPE_RICE:
                return getDistributionRice();
        }
        return 0;
    }
}
