package com.nasaspaceapps.foodmatch;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;


public class ChooseFood extends Fragment implements View.OnClickListener{


    private String type;

    private ImageView ibtnRice;
    private ImageView ibtnCorn;
    private ImageView ibtnWheat;
    private ImageView ibtnPalmOil;

    private static final String riceNormalUrl = "https://scontent.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/17029_10205245600156904_5369496265175893116_n.jpg?oh=d9765c9ecb53a9689cb04c92b751dd8b&oe=55E429AD";
    private static final String cornNormalUrl = "https://scontent.xx.fbcdn.net/hphotos-xfp1/v/t1.0-9/11053133_10205245629357634_4188558115784790207_n.jpg?oh=14db066f900ba35f4ad9349218ddfc77&oe=55B656B1";
    private static final String wheatNormalUrl = "https://scontent.xx.fbcdn.net/hphotos-xpt1/v/t1.0-9/11150557_10205245653558239_2106094387693866574_n.jpg?oh=c93dfbc604f8d97160a5248b807311ab&oe=55A39622";
    private static final String palmOilNormalUrl = "https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-xfa1/v/t1.0-9/17027_10205245600676917_8438070998494884070_n.jpg?oh=b19be13e3eec91d715fe247bf6613398&oe=55B5999F&__gda__=1437661135_181bcbe03dbc26d44b4d0f77883fea69";

    //BLURED
    private static final String riceBLurUrl = "https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-xfp1/v/t1.0-9/988515_10205245600196905_807595819120408497_n.jpg?oh=264991428cc1e694cd337af5e3fec274&oe=5599E701&__gda__=1436156570_d7c51d0c7062dadf3516afb35953494c";
    private static final String cornBLurUrl = "https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xap1/v/t1.0-9/11083624_10205245629317633_1510298360575191506_n.jpg?oh=e62c6adc901f74a6d40ba41382cb0c41&oe=55E40A0A&__gda__=1437358943_49a46279c419d3819d6b9221ef647c5a";
    private static final String wheatBLurUrl = "https://scontent.xx.fbcdn.net/hphotos-xat1/v/t1.0-9/11128760_10205245653518238_2645653063552984464_n.jpg?oh=e11bcd86573e5f0f3579a2f7d8b960f7&oe=55AE3D8D";
    private static final String palmOilBLurUrl = "https://scontent.xx.fbcdn.net/hphotos-xfp1/v/t1.0-9/10395175_10205245600596915_6567774640894557823_n.jpg?oh=f1ba85b776a238bb0fcd22b17be7cba8&oe=55DF9C9B";

    private Button startMatching;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_choose_food, container, false);

        startMatching = (Button)rootView.findViewById(R.id.btn_findMatch);
        startMatching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity)getActivity();
                activity.foodType = type;
                activity.addFragment(new MakeMatch());
            }
        });

        ibtnRice  = (ImageView)rootView.findViewById(R.id.iv_rice);
        ibtnCorn  = (ImageView)rootView.findViewById(R.id.iv_corn);
        ibtnWheat = (ImageView)rootView.findViewById(R.id.iv_Wheat);
        ibtnPalmOil = (ImageView)rootView.findViewById(R.id.iv_palm_oil);

        ibtnRice.setOnClickListener(this);
        ibtnCorn.setOnClickListener(this);
        ibtnWheat.setOnClickListener(this);
        ibtnPalmOil.setOnClickListener(this);

        loadAllBlured();
        type = Country.TYPE_CORN;
        loadCorn();
        return rootView;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.iv_rice:
                type = Country.TYPE_RICE;
                loadRice();
                break;
            case R.id.iv_corn:
                type = Country.TYPE_CORN;
                loadCorn();
                break;
            case R.id.iv_Wheat:
                type = Country.TYPE_WHEAT;
                loadWheat();
                break;
            case R.id.iv_palm_oil:
                type = Country.TYPE_PALM_OIL;
                loadPalmOil();
                break;
        }

    }

    private void loadAllBlured(){
        Glide.with(getActivity().getBaseContext()).load(riceBLurUrl).crossFade().into(ibtnRice);
        Glide.with(getActivity().getBaseContext()).load(wheatBLurUrl).crossFade().into(ibtnWheat);
        Glide.with(getActivity().getBaseContext()).load(cornBLurUrl).crossFade().into(ibtnCorn);
        Glide.with(getActivity().getBaseContext()).load(palmOilBLurUrl).crossFade().into(ibtnPalmOil);
    }

    private void loadRice(){
        loadAllBlured();
        Glide.with(getActivity().getBaseContext()).load( riceNormalUrl ).crossFade().into(ibtnRice);
    }

    private void loadCorn(){
        loadAllBlured();
        Glide.with(getActivity().getBaseContext()).load( cornNormalUrl ).crossFade().into(ibtnCorn);
    }

    private void loadWheat(){
        loadAllBlured();
        Glide.with(getActivity().getBaseContext()).load( wheatNormalUrl ).crossFade().into(ibtnWheat);
    }

    private void loadPalmOil(){
        loadAllBlured();
        Glide.with(getActivity().getBaseContext()).load(palmOilNormalUrl).crossFade().into(ibtnPalmOil);
    }
}
