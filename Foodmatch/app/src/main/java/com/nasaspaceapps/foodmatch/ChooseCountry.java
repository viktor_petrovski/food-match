package com.nasaspaceapps.foodmatch;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class ChooseCountry extends Fragment {

    private TextView countryNameTV;
    private ImageView countryFlagIV;
    private Spinner countrySpinner;
    private Button pickButton;
    ArrayList<Country> countries = new ArrayList<>();

    public ChooseCountry() {
        // Required empty public constructor

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_choose_country, container, false);

        countryNameTV = (TextView)rootView.findViewById(R.id.tv_countryName);
        countryFlagIV = (ImageView)rootView.findViewById(R.id.iv_country_flag);
        countrySpinner = (Spinner) rootView.findViewById(R.id.spinner_countries);
        pickButton = (Button) rootView.findViewById(R.id.btn_pickCountry);
        pickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.choosenCountry = country;
                mainActivity.addFragment(new ChooseFood());
            }
        });
        initCountries();
        final ArrayAdapter<Country> countryArrayAdapter   = new ArrayAdapter<Country>(getActivity().getBaseContext(), android.R.layout.simple_dropdown_item_1line,countries);
        countrySpinner.setAdapter(countryArrayAdapter);
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setCountry(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        setCountry(0);
        return rootView;
    }

    private void initCountries(){
        countries.add(new Country("USA","http://obsessivemommy.com/wp-content/uploads/2013/06/american-flag.png",633000,1247000,7068000,4354000));
        countries.add(new Country("Japan","http://upload.wikimedia.org/wikipedia/en/thumb/9/9e/Flag_of_Japan.svg/1280px-Flag_of_Japan.svg.png",15400000,620000,7679000,6000000));
        countries.add(new Country("Germany","http://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/1280px-Flag_of_Germany.svg.png",166055,788100,386325,4410957));
        countries.add(new Country("China","http://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People's_Republic_of_China.svg/2000px-Flag_of_the_People's_Republic_of_China.svg.png",2500,6300,144500,1500));
    }
    Country country;
    private void setCountry(int position){

        country = countries.get(position);
        countryNameTV.setText(country.getCountryName());
        Glide.with(getActivity().getBaseContext()).load(country.getCountryFlagUrl()).into(countryFlagIV);

    }

}

