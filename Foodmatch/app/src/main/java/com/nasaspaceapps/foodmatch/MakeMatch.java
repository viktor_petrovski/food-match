package com.nasaspaceapps.foodmatch;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MakeMatch extends Fragment {

    public MakeMatch() {
        // Required empty public constructor
    }

    Button makeMatch;
    Country choosenCountry;
    String type;
    TextView tv;

    ImageView ctrCountryOne;
    ImageView ctrCountryTwo;

    String string = "The moment Africa's potential reaches global awareness,will be the moment every meal reaches its destination";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_make_match, container, false);
        populateCountries();

        tv= (TextView)rootView.findViewById(R.id.tv_match_countries);
        makeMatch = (Button)rootView.findViewById(R.id.btn_make_new_match);
        makeMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity)getActivity();
                activity.addFragment(new ChooseCountry());
            }
        });

        TextView tekstBoks = (TextView)rootView.findViewById(R.id.tv_main_description);
        tekstBoks.setText(string);
        choosenCountry = ((MainActivity) getActivity()).choosenCountry;
        type = ((MainActivity) getActivity()).foodType;

        ctrCountryOne = (ImageView) rootView.findViewById(R.id.iv_country_one);
        ctrCountryTwo = (ImageView) rootView.findViewById(R.id.iv_country_two);

        Collections.sort(countries, new Comparator<Country>() {
            @Override
            public int compare(Country lhs, Country rhs) {
                return (int) (rhs.avg(type) -lhs.avg(type));
            }
        });


        calculate();
        tv.setText(choosenCountry.getCountryName()+" & " + countryMatch.getCountryName());
        Glide.with(this).load(choosenCountry.getCountryFlagUrl()).transform(new CircleTransform(getActivity().getBaseContext())).into(ctrCountryOne);
        Glide.with(this).load(countryMatch.getCountryFlagUrl()).transform(new CircleTransform(getActivity().getBaseContext())).into(ctrCountryTwo);


        return rootView;
    }

    public static Bitmap getCircularBitmapImage(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());
        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;
        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }
        Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);
        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);
        squaredBitmap.recycle();
        return bitmap;
    }

    public class CircleTransform extends BitmapTransformation {
        private Context context;

        public CircleTransform(Context context) {
            super(context);
            this.context = context;
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap source, int outWidth, int outHeight) {
            return getCircularBitmapImage(source);
        }

        @Override
        public String getId() {
            return "Glide_Circle_Transformation";
        }
    }



    Country countryMatch;
    private void calculate(){
        StringBuilder sb = new StringBuilder();
        sb.append("Choosen Country "+choosenCountry.getCountryName() + " in type " + type +" "+ choosenCountry.getDistribution(type)+"\n");

        int country = 1;
        countryMatch = countries.get(0);
        for (Country c : countries){
            sb.append( country ++);
            sb.append(" "+ c.getCountryName() + " with avg: "+ c.avg(type)+ " \n");
            if(choosenCountry.getDistribution(type) - countryMatch.avg(type) < choosenCountry.getDistribution(type) - c.avg(type))
                countryMatch = c;
        }
        sb.append(" MATCHED COUNTRY : \n\n\n"+ countryMatch.getCountryName());

        //tv.setText(sb.toString());
    }

    ArrayList<Country> countries = new ArrayList<>();

    private void populateCountries(){
        long defaultValue = 0;

       countries.add(new Country("Nigeria","http://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Flag_of_Nigeria.svg/1920px-Flag_of_Nigeria.svg.png",2604000,2604000,17880800,defaultValue,
               86830000,5500000,17625000,defaultValue));
       countries.add(new Country("Senegal","http://countries.bridgat.com/images/Senegal_Flag.jpg",211950,defaultValue,1955390,defaultValue,
                3600000,defaultValue,1500000,defaultValue));
       countries.add(new Country("Mali","http://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flag_of_Mali.svg/1280px-Flag_of_Mali.svg.png",229500,defaultValue,1575900,defaultValue,
                    8100000,defaultValue,1500000,defaultValue));
        countries.add(new Country("Ghana","http://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Flag_of_Ghana.svg/2000px-Flag_of_Ghana.svg.png",229500,defaultValue,1575900,defaultValue,
                15736059,defaultValue,2291965,defaultValue));
//        countries.add(new Country("Benin","",159800,154800,defaultValue,defaultValue,
//                defaultValue,defaultValue,defaultValue,defaultValue));
        countries.add(new Country("Chad","http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Flag_of_Chad.svg/2000px-Flag_of_Chad.svg.png",defaultValue,defaultValue,1321490,defaultValue,
                defaultValue,defaultValue,1237500,defaultValue));
        countries.add(new Country("Kenya","http://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Kenya.svg/2000px-Flag_of_Kenya.svg.png",665250,defaultValue,defaultValue,4124550,
                16537171,defaultValue,defaultValue,19433840));

    }
}
