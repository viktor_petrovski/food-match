package com.nasaspaceapps.foodmatch;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

   public Country choosenCountry;
   public String foodType;

    public void addFragment(Fragment f){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, f)
                .commit();
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {

        private String[] africaDescriptions =
        {"Nearly one-fourth of the world’s agricultural land is in Africa but is not being worked to its full potential.",
         "More than 24 percent of the population in sub-Saharan Africa is malnourished. Bringing production of just 16 key crops up to their potential could yield more than 205 million additional metric tons of food.",
         "With modern farming techniques and programs to help farmers afford them, this potential breadbasket might not only feed itself but also export a surplus"
        };

        private int currentDescription;
        private ImageView backgroundImageView;
        private TextView descriptionHolder;
        private static final String backgroundImageUrl = "http://upload.wikimedia.org/wikipedia/en/2/21/Africa_satellite_orthographic.jpg";
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            backgroundImageView = (ImageView)rootView.findViewById(R.id.iv_main_background);
            descriptionHolder = (TextView)rootView.findViewById(R.id.tv_description_text);
            backgroundImageView = (ImageView)rootView.findViewById(R.id.iv_main_background);
            descriptionHolder.setOnClickListener(this);
            currentDescription = 0;

            Glide.with(getActivity().getBaseContext()).load(backgroundImageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(backgroundImageView);
            descriptionHolder.setText(africaDescriptions[currentDescription]);
            return rootView;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tv_description_text:
                    switchText();
                    break;
            }
        }

        private void switchText(){
            currentDescription ++;
            if(currentDescription == africaDescriptions.length)
                proceedToChooseCountry();
            else
                descriptionHolder.setText(africaDescriptions[currentDescription]);
        }

        private void proceedToChooseCountry(){
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.addFragment(new ChooseCountry());
        }

    }
}
